# web21
## @edt ASIX-M05 Curs 2021-2022

### Descripció

Generar una imatge web21 amb un servidor web apache basada en debian (latest). Aquesta imatge simplement 
incorpora el paquet Apache però NO cap pàgina web. Així en engegar-se per un container mostrarà la pàgina
web per defecte de Apache.

L'objectiu és desenvolupar a part, en el host amfitrió, la pàgina web i tenir-la 'muntada', integrada, 
dins del container. Això no afecta al desenvolupament de la imatge sinó a la execució del container.

En aquesta imatge s'ha modificat el Dockerfile fent:
 * Ja no copia la pàgina index.html, no existeix.

Fitxers que utilitza:
 * Dockerfile



#### Generar imatge

```
docker build -t edtasixm05/web21:develop .
```

#### Executar en detach

```
docker run --rm --name web -h web -d edtasixm05/web21:develop
```


#### Verificar-ne el funcionament

Observar que està engegat el container, els processos que s'hi executen i els ports

```
$ docker run --rm --name web  -h web   -d edtasixm05/web21:develop
55e37107b76fbaf4148a314ed811880c80f2a4dd31a70ab3b531d6ca15536e73

$ docker ps
CONTAINER ID   IMAGE                      COMMAND                  CREATED         STATUS         PORTS     NAMES
55e37107b76f   edtasixm05/web21:develop   "/bin/sh -c 'apachec…"   4 seconds ago   Up 3 seconds   80/tcp    web
```

```
$ nmap 172.17.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 20:05 CEST
Nmap scan report for ldap.edt.org (172.17.0.2)
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

Descarregar la pàgina web

```
$ wget 172.17.0.1
--2022-05-05 20:05:40--  http://172.17.0.1/
Connecting to 172.17.0.1:80... failed: Connection refused.
[ecanet@mylaptop web21:develop]$ wget 172.17.0.2
--2022-05-05 20:05:43--  http://172.17.0.2/
Connecting to 172.17.0.2:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 10701 (10K) [text/html]
Saving to: ‘index.html’

index.html                              100%[=============================================================================>]  10.45K  --.-KB/s    in 0s      

2022-05-05 20:05:43 (468 MB/s) - ‘index.html’ saved [10701/10701]
```

```
Des d'un navegador web accedir a l'adreça 172.17.0.2 i observar la pàgina web per defecte del servei Apache
```


### Aturar el container del servei web

Recordeu d'aturar el container i eliminar-lo (es fa automàticament si s'ha fet el run amb *--rm*) per poder tornar a 
engegar-ne un amb el mateix nom.

```
$ docker stop web
web

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

```


---

---


## Utilització de *bind mounts* i *Volumes*


Podeu consultar la documentació de [Docker Run CLI Mount Volume](https://docs.docker.com/engine/reference/commandline/run/#mount-volume--v---read-only)


L'objectiu d'aquesta demostració es desplegar el container i que aquest utilitzi o un fitxer, 
o un directori o un volum del host anfitrió dins seu. Això no es defineix en generar la imatge sinó en temps 
d'execució en crear i engegar el container amb l'ordre *docker run*

**Important!**. Haureu observat que les dades de dins dels containers **no tenen persistència**. Les que formen part
de la imatge si, però totes aquelles dades que es transformin o generin en l'execució del container es perden quan el 
container es destrueix (no en aturar-se). Si es vol persistència de dades caldrà que les dades es desin externament, 
per fer-ho hi ha diverses tècniques, dues d'elles són els *bind mounts* i els *volumes*.


### Bind Mount

Podeu consultar la documentació de [Docker how to use Bind Mounts](https://docs.docker.com/storage/bind-mounts/)

Podeu consultar la documentació de [Docker Run CLI Bind Mounts](https://docs.docker.com/engine/reference/commandline/run/#add-bind-mounts-or-volumes-using-the---mount-flag)


En aquest exemple farem que en enegar el container s'utilitzi el directori */tmp/myweb* del host anfitriò com  directori
*/var/www/html* dins del container. La idea és que desenvolupem la nostra complexa, sofisticada i super difícil web en el
directori de desenvolupament */tmp/myweb* i la provem a través del servidor web Apache del container. Els canvis que anem
fent en calent als fitxers del directori de desenvolupament del host anfitrió s'aniran reflexant el la web publicada.

Aquest és un mecanisme de poder desenvolupar un contingut i provar-lo de manera *'inocua'* no només en un servidor web 
sinó en múltples, per exemple testejant en container amb versions diferents del servei Apache o d'altres servidors.


Generem el directori de desenvolupament i posem la pàgina web
```
$ mkdir /tmp/myweb

$ 
$ cat > index.html 
<html>
    <head>
        <title>ASIX-M05 Dockers</title>
    </head>
    <body>
        <h1>web 1hisix</h1>
        <h2>practica M05 docker</h2>
        hola soc la pagina web
	d'exemple de ASIX-M05 dockers
    </body>
</html>
```

#### Exemple-1: Bind Mount d'un fitxer

```
# Recordeu d'aturar el container si està engegat
# docker stop web

$ docker run --rm --name web  -h web  --mount type=bind,src=/tmp/myweb/index.html,dst=/var/www/html/index.html -d edtasixm05/web21:develop
1dc8e080ce633db07c25508afb7a21b9ae3b97e37e9e6442a1b853a14fca37d1

$ docker ps
CONTAINER ID   IMAGE                      COMMAND                  CREATED         STATUS         PORTS     NAMES
1dc8e080ce63   edtasixm05/web21:develop   "/bin/sh -c 'apachec…"   4 seconds ago   Up 2 seconds   80/tcp    web

$ docker container inspect web
```

Verificar que la pàgina web és la que estem desenvolupant

```
$ telnet 172.17.0.2
Trying 172.17.0.2...
telnet: connect to address 172.17.0.2: Connection refused
[ecanet@mylaptop web21:develop]$ telnet 172.17.0.2 80
Trying 172.17.0.2...
Connected to 172.17.0.2.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Thu, 05 May 2022 18:33:36 GMT
Server: Apache/2.4.53 (Debian)
Last-Modified: Thu, 05 May 2022 18:24:39 GMT
ETag: "e4-5de47dada91ea"
Accept-Ranges: bytes
Content-Length: 228
Vary: Accept-Encoding
Connection: close
Content-Type: text/html

<html>
    <head>
        <title>ASIX-M05 Dockers</title>
    </head>
    <body>
        <h1>web 1hisix</h1>
        <h2>practica M05 docker</h2>
        hola soc la pagina web
	d'exemple de ASIX-M05 dockers
    </body>
</html>
Connection closed by foreign host.
```

Passem a fer modificacions a la pàgina web i observem en calent que aquestes es reflecteixen, per exemple,
en el navegador

```
 cat index.html 
<html>
    <head>
        <title>ASIX-M05 Dockers</title>
    </head>
    <body>
        <h1>web 1hisix</h1>
        <h2>practica M05 docker</h2>
        hola soc la pagina web
	d'exemple de ASIX-M05 dockers
	</p>
	Aquest paràgraf és una modificació feta amb
	un bind mount
    </body>
</html>
```

```
Des d'un navegador web accedir a l'adreça 172.17.0.2 i observar el nou contingut de la pàgina,
que s'ha modificat sense recarregar en cap moment el servei web
```

Observem que dins el container hi ha la pàgina web modificada. Bé, de fet no hi és sinó que hi és muntada...

```
$ docker exec -it web ls -l /var/www/html
total 4
-rw-rw-r--. 1 1001 1001 297 May  5 18:42 index.html
```

```
$ docker exec -it web mount -t tmpfs
...
tmpfs on /var/www/html/index.html type tmpfs (rw,nosuid,nodev,seclabel,inode64)
...
```


#### Exemple-2: Bind Mount d'un directori


Realitzem el mateix exemple però ara fent un Bind Mount del directori */tmp/myweb* del host amfitrió al directori
*/var/www/html* del container. Tot el que es modifiqui en aquest directori del host amfitrió s'està modificant
*en calent* en el container.

```
# Recordeu d'aturar el container si està engegat
# docker stop web

$ docker run --rm --name web  -h web  --mount type=bind,src=/tmp/myweb,dst=/var/www/html -d edtasixm05/web21:develop
1fd39d6599b0aa2f7624117f7fabd689109fa0470310a3465fcbd460f7fdf4e9

$ docker ps
CONTAINER ID   IMAGE                      COMMAND                  CREATED         STATUS         PORTS     NAMES
1fd39d6599b0   edtasixm05/web21:develop   "/bin/sh -c 'apachec…"   5 seconds ago   Up 4 seconds   80/tcp    web

$ docker exec -it web ls -l /var/www/html
-rw-rw-r--. 1 1001 1001 297 May  5 18:42 index.html

$ docker exec -it web mount -t tmpfs
...
tmpfs on /var/www/html type tmpfs (rw,nosuid,nodev,seclabel,inode64)
...
```


### Mount Volumes


Podeu consultar la documentació de [Docker Use Volumnes](https://docs.docker.com/storage/volumes/)

Podeu consultar la documentació de [Docker Run CLI Mount Volume](https://docs.docker.com/engine/reference/commandline/run/#mount-volume--v---read-only)


Un mètode alternatiu als Bind Mounts (i més pràctic...) són els volums. Un volum és un espai 
d'emmagatzemamanet que resideix en el host amfitrió i que és utilitzable dins del container. 
Ens podem imaginar els volums com si fossin unitats de USB que en el container es munten en 
el directori indicat. L'avantatge dels volums és que docker en permet una gestió a través de 
l'ordre **docker volume** iaixí com que se'n poden fer còpies de seguretat fàcilment.


#### Gestió de volumes amb docker volume

Podem crear, llistar i eliminar volums amb *docker volume*

```
$ docker volume create volume1
volume1

$ docker volume ls
DRIVER    VOLUME NAME
local     minikube
local     volume1

$ docker volume create volume2
volume2

$ docker volume ls
DRIVER    VOLUME NAME
local     minikube
local     volume1
local     volume2

$ docker volume 
create   inspect  ls       prune    rm       

$ docker volume rm volume2
volume2

# docker volume inspect volume1

$ docker volume prune
WARNING! This will remove all local volumes not used by at least one container.
Are you sure you want to continue? [y/N] y
Deleted Volumes:
volume1
minikube
Total reclaimed space: 1.324GB
```

#### Exemple-3: Container amb un Volume usant -v

En aquest exemple anem a crear un volum anomenat *web-data* que contindrà la seu web que estem desenvolupant. 
Dins d'aquest volum hi posarem  la pàgina web (i tot allò que faci falta).

```
$ docker volume create web-data
web-data

$ docker volume ls
DRIVER    VOLUME NAME
local     web-data

$ docker volume inspect web-data 
[
    {
        "CreatedAt": "2022-05-05T21:04:20+02:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/web-data/_data",
        "Name": "web-data",
        "Options": {},
        "Scope": "local"
    }
]

[root@mylaptop ~]# ls /var/lib/docker/volumes/web-data/
_data

[root@mylaptop ~]# tree /var/lib/docker/volumes/web-data/
/var/lib/docker/volumes/web-data/
└── _data
```

Engegar el container amb el volum *web-data* associat al directori */var/www/html*. Ara, però,
el directori originalment està buit i en instal·lar-se el paquet apache s'hi genera el fitxer
index.html per defecte de Apache (observeu-ne la mida).

```
# Recordeu d'aturar el container si està engegat
# docker stop web

$ docker run --rm --name web  -h web  -v web-data:/var/www/html -d edtasixm05/web21:develop
3192d0ce1f92983631850d3afdde6288927541b322115bf07f93ef9b85c94057

$ docker ps
CONTAINER ID   IMAGE                      COMMAND                  CREATED         STATUS         PORTS     NAMES
3192d0ce1f92   edtasixm05/web21:develop   "/bin/sh -c 'apachec…"   6 seconds ago   Up 5 seconds   80/tcp    web

$ docker exec -it web ls -l /var/www/html
total 12
-rw-r--r--. 1 root root 10701 May  5 16:19 index.html
```

Si accedim a la web veurem que mostra la pàgina per defcete del servidor Apache.
```
Des del navegador web accedir a l'adreça 172.17.0.2
```

Ara tots els canvis que es facin dins del container perduraran. **Atenció!** en els exemples anteriors les dades
es podien editar des de l'exterior (el host amfitrió). Ara les dades perduren en el volum, però s'han d'editar 
des de dins del container. Un exemple de funcionament dels volums és un container amb una base de dades Postgres
o una base de dades ldap, on les dades han de perdurar (les altes, baixes i modificacios) encara que el container
finalitzi. En aquest cas les dades perduren entre execucions de containers nous a través del volum.

Si es vol modificar les dades del volum des de fora es pot optar per:
 * Fer un docker exec per des d'una altra sessió modificar les dades
 * Des del Linux es pot muntar el fitxer del volum (/var/lib/docker/volumes/web-data) al loop i treballar-hi.


**Modificar el contingut del volum**

Anem a modificar la pàgina web del volum, usant **docker cp**, mentre el container està engegat. Eliminarem
el fitxer index.html per defecte d'Apache i posarem el nostre. Automàticament la web passarà a ser la nostra.

```
$ docker cp /tmp/myweb/index.html web:/var/www/html/index.html
[ecanet@mylaptop web21:develop]$ docker exec -it web cat /var/www/html/index.html
<html>
    <head>
        <title>ASIX-M05 Dockers</title>
    </head>
    <body>
        <h1>web 1hisix</h1>
        <h2>practica M05 docker</h2>
        hola soc la pagina web
	d'exemple de ASIX-M05 dockers
	</p>
	Aquest paràgraf és una modificació feta amb
	un bind mount
    </body>
</html>
```
```
Des d'un navegador web accedir a l'adreça 172.17.0.2 i observar que la pàgina web ja
no és la del servei Apache per defecte sinó la nostra
```

**Nota sobre la persistència de dades**

Observeu que si finalitzem el conatiner, com que s'ha engegat amb l'opció *--rm*  es destruirà. Podriem pensar
que en engegar un nou container les dades del volum perduraran, i és realment així, però tenim el problema que
en la creació del container la instal·lació del paquet Apache podria sobreescriure el fitxer index.html (sembla 
que en aquest cas no ho fa, però hem d'entendre que en altres tipus d'instal·lacions ens trobarem quest problema)

Aquest problema caldrà resoldre'l més endavant amb la utilització d'entry-points que permetin detectar
si el container ha de crear de nou la seu web o n'ha d'utilitzar una de ja existent.

```
$ docker run --rm  --name web  -h web  -v web-data:/var/www/html -d edtasixm05/web21:develop
f50e611bd90dfeb6a40d26e6b52d5fb3e75af4e59fb921ac20e7aa690b0de9da
[ecanet@mylaptop web21:develop]$ docker exec -it web cat /var/www/html/index.html
<html>
    <head>
        <title>ASIX-M05 Dockers</title>
    </head>
    <body>
        <h1>web 1hisix</h1>
        <h2>practica M05 docker</h2>
        hola soc la pagina web
	d'exemple de ASIX-M05 dockers
	</p>
	Aquest paràgraf és una modificació feta amb
	un bind mount
    </body>
</html>
```


#### Exemple-4: Volumes usant --mount

Una sintaxi alternativa per usar volums és utilitzar l'opció *--mount* que actualment
tendeix a generalitzar-se perquè serveix tant per a volums com per a bind mounts.

```
# Recordeu d'aturar el container si està engegat
# docker stop web

$ docker run --rm --name web  -h web  --mount source=web-data,target=/var/www/html -d edtasixm05/web21:develop
318920a5383134a220398e0843f9a9b972f5c031c82f59a400d0382f84c7e57f

$ docker ps
CONTAINER ID   IMAGE                      COMMAND                  CREATED          STATUS          PORTS     NAMES
318920a53831   edtasixm05/web21:develop   "/bin/sh -c 'apachec…"   19 seconds ago   Up 18 seconds   80/tcp    web

$ docker container inspect web
...
"Mounts": [
            {
                "Type": "volume",
                "Name": "web-data",
                "Source": "/var/lib/docker/volumes/web-data/_data",
                "Destination": "/var/www/html",
                "Driver": "local",
                "Mode": "z",
                "RW": true,
                "Propagation": ""
            }
        ],
...
```
 

