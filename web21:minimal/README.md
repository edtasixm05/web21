# web21
## @edt ASIX-M05 Curs 2021-2022

### Descripció

Generar una imatge web21 amb un servidor web apache basada en debian (latest). Segueix un procediment
similar al de les imatges anteriors però és més *lleugera*. En aquesta ocasió s'han fet els canvis següents
el el Dockerfile:

 * Des del Dockerfile es copia la pàgina web a la seva ubicació final
 * No es prepara cap *'entorn especial'* com el directori */opt/docker* que hem usat acadèmicament
   en els exemples anteriors.
 * El CMD a exutar és directament el servei web apache en foreground.
 * Només s'instal·la el paquet *apache2*, els altres paquets que utilitzavem per a la depuració no
   s'han instal·lat.

Fitxers que utilitza:
 * Dockerfile
 * index.html


#### Generar imatge

```
docker build -t edtasixm05/web21:minimal .
```

#### Executar en detach

```
docker run --rm --name web -h web -d edtasixm05/web21:minimal
```

#### Verificar-ne el funcionament

Observar que està engegat el container, els processos que s'hi executen i els ports

```
$ docker run --rm --name web  -h web  -d edtasixm05/web21:minimal
62f445d37c8373d41a7a2ec578adbd7e4ff5394d1f145f21bf9ef2ccd5d5175d

$ docker ps
CONTAINER ID   IMAGE                      COMMAND                  CREATED         STATUS         PORTS     NAMES
62f445d37c83   edtasixm05/web21:minimal   "/bin/sh -c 'apachec…"   3 seconds ago   Up 2 seconds   80/tcp    web
```

```
$ nmap 172.17.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 18:24 CEST
Nmap scan report for ldap.edt.org (172.17.0.2)
Host is up (0.000098s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

```
 docker top web
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                21471               21451               0                   18:20               ?                   00:00:00            /bin/sh -c apachectl -k start -X
root                21509               21471               0                   18:20               ?                   00:00:00            /bin/sh /usr/sbin/apachectl -k start -X
33                  21518               21509               0                   18:20               ?                   00:00:00            /usr/sbin/apache2 -k start -X
```

Descarregar la pàgina web

```
$ wget 172.17.0.2 
```

```
$ telnet 172.17.0.2 80
Trying 172.17.0.2...
Connected to 172.17.0.2.
Escape character is '^]'.
GET / HTTP/1.0
```

```
Des d'un navegador web accedir a l'adreça: 172.17.0.2
```

### Aturar el container del servei web

Recordeu d'aturar el container i eliminar-lo (es fa automàticament si s'ha fet el run amb *--rm*) per poder tornar a 
engegar-ne un amb el mateix nom.

```
$ docker stop web
web

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

```


#### Informació de la imatge

```
$ docker history edtasixm05/web21:minimal 
IMAGE          CREATED             CREATED BY                                      SIZE      COMMENT
686fe16c1f4b   7 minutes ago       /bin/sh -c #(nop)  EXPOSE 80                    0B        
0c94a47c58e5   7 minutes ago       /bin/sh -c #(nop)  CMD ["/bin/sh" "-c" "apac…   0B        
ea4900827476   7 minutes ago       /bin/sh -c #(nop) WORKDIR /tmp                  0B        
39785dc6ab7c   7 minutes ago       /bin/sh -c apt-get update && apt-get -y inst…   136MB     
b65141538e08   About an hour ago   /bin/sh -c #(nop)  LABEL author=@edt ASIX M05   0B        
2a3f815efd5f   About an hour ago   /bin/sh -c #(nop)  LABEL subject=webserver      0B        
82bd5ee7b1c5   8 months ago        /bin/sh -c #(nop)  CMD ["bash"]                 0B        
<missing>      8 months ago        /bin/sh -c #(nop) ADD file:1fedf68870782f1b4…   124MB     
```

```
docker image inspect edtasixm05/web21:minimal
```

---

---


## Propagació del port


Podeu consultar la documentació de *docker run--publish* a [Docker Documentation CLI Reference](https://docs.docker.com/engine/reference/commandline/run/#publish-or-expose-port--p---expose)


La imatge s'ha generat indicant que s'exporta el port 80 amb l'ordre *EXPOSE 80*, però a hores d'ara aquesta directiva no l'estem aplicant.
En crear el container en la xarxa per defecte *172.17.0.0/16* sabem que:

 * El *host* amfitrió té l'adreça **172.17.0.1/16** en una interfície anomenada **docker0**.
 * Els container que es creen en la xarxa per defecte tenen adreces correlatives dins d'aquesta xarxa, el primer
   la 172.17.0.2/16 el segon 172.17.0.3/16 i així sucsessivament. L'inconvenient, però és que si s'apagen i engegen
   les adreces IP es reutilitzen de manera que no es pot garantir quina adreça IP té el container.

Respecte el servei web que porporciona *Apache* sabem que obre el port 80 del container de manera que fins ara ha sigut a través
d'aquest port que s'ha accedit al servei. Però la idea dels containers és oferir *micro serveis* (un container un servei) 
con si s'executessin en el host amfitrió.

Podem observar que a hores d'ara el container oferix el port 80, però el host amfitriò no (atenció!: verifiqueu que no teniu
en el vostre host algun altre servei web engegat)

```
$ nmap 172.17.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 18:38 CEST
Nmap scan report for ldap.edt.org (172.17.0.2)
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

```
$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 18:38 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00010s latency).
All 1000 scanned ports on 172.17.0.1 are closed

$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 18:38 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000095s latency).
Other addresses for localhost (not scanned): ::1
All 1000 scanned ports on localhost (127.0.0.1) are closed
```


**Així, doncs, l'objectiu és que el container propagi el seu port al host amfitrió**, de manera que en accedir al port
80 del host en realitat respondrà el servei web del port 80 del container. Per realitzar aquesta tasca cal engegar
el container fent una redirecció de ports.


**Opcions de propagació de ports**

```
 -p port-host:port-container
 -p ip-host:port-host:port-container
 -P 
```

#### Exemple-1: Propagar port 80:80

En aquest exemple el container propaga el seu port 80 al port 80 del host, a totes les seves interfícies.

Observem que en el container el servei està obert:
```
# Recordeu de fer el 'stop' del container si encara el teniu engegat
# docker stop web

$ docker run --rm --name web  -h web  -p 80:80 -d edtasixm05/web21:minimal
0644337bebe8f4d2a3bc290c5dc8fab5b3decbcff774972020ccd73660005d73

$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 18:43 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00010s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```


Observem que a totes les interfícies del host amfitrió el port 80 s'ha obert:
```
$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 18:43 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00010s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 18:45 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00010s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

$ nmap 192.168.1.45
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 18:45 CEST
Nmap scan report for 192.168.1.45
Host is up (0.00010s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

Observem que en tots els casos accedir al port 80 del host proporciona la pàgina web del servei Apache
del container:

```
$ telnet localhost 80
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Thu, 05 May 2022 16:46:45 GMT
Server: Apache/2.4.53 (Debian)
Last-Modified: Thu, 05 May 2022 16:14:03 GMT
ETag: "e4-5de4607c548c0"
Accept-Ranges: bytes
Content-Length: 228
Vary: Accept-Encoding
Connection: close
Content-Type: text/html

<html>
    <head>
        <title>ASIX-M05 Dockers</title>
    </head>
    <body>
        <h1>web 1hisix</h1>
        <h2>practica M05 docker</h2>
        hola soc la pagina web
	d'exemple de ASIX-M05 dockers
    </body>
</html>
Connection closed by foreign host.


 wget 172.17.0.1
--2022-05-05 18:47:06--  http://172.17.0.1/
Connecting to 172.17.0.1:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 228 [text/html]
Saving to: ‘index.html.2’

index.html.2                            100%[=============================================================================>]     228  --.-KB/s    in 0s      

2022-05-05 18:47:06 (69.8 MB/s) - ‘index.html.2’ saved [228/228]

# Usant un navegador accedir a l'adreça IP pública del host, en aquest cas 192.168.1.45

```


#### Exemple-2: Propagar el port 80 del container al port 5000 del host


Ara el servei web s'oferirà en el port 5000 de totes les interfícies del host. De manera que quan un client
accedeixi a aquest port del host amfitriò en realitat contestarà el port 80 del container, la nostra pàgina web.

Engegar el container amb la propagació de ports
```
# Recordeu de fer el 'stop' del container si encara el teniu engegat
# docker stop web

$ docker run --rm --name web  -h web  -p 5000:80 -d edtasixm05/web21:minimal
75ed31ab0ca6946fb21e7aba146713fe3f1ebe06551b3970b7b85ecd0a23ad3e

$ nmap 172.17.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:02 CEST
Nmap scan report for ldap.edt.org (172.17.0.2)
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

Observar les interfícies del host amfitrió
```
$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:03 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00010s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp

$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:03 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000092s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp

$ nmap 192.168.1.45
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:03 CEST
Nmap scan report for 192.168.1.45
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp
```

Verificar que permeten accedir a la web
```
$ telnet localhost 5000
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Thu, 05 May 2022 17:04:55 GMT
Server: Apache/2.4.53 (Debian)
Last-Modified: Thu, 05 May 2022 16:14:03 GMT
ETag: "e4-5de4607c548c0"
Accept-Ranges: bytes
Content-Length: 228
Vary: Accept-Encoding
Connection: close
Content-Type: text/html

<html>
    <head>
        <title>ASIX-M05 Dockers</title>
    </head>
    <body>
        <h1>web 1hisix</h1>
        <h2>practica M05 docker</h2>
        hola soc la pagina web
	d'exemple de ASIX-M05 dockers
    </body>
</html>
Connection closed by foreign host.
```
```
$ wget 172.17.0.1:5000
--2022-05-05 19:05:24--  http://172.17.0.1:5000/
Connecting to 172.17.0.1:5000... connected.
HTTP request sent, awaiting response... 200 OK
Length: 228 [text/html]
Saving to: ‘index.html.1’

index.html.1                            100%[=============================================================================>]     228  --.-KB/s    in 0s      

2022-05-05 19:05:24 (57.0 MB/s) - ‘index.html.1’ saved [228/228]
```
```
Des d'un navegador web accedir a l'adreá http://192.168.1.45:5000
```


#### Exemple-3: Propagar un port dinàmicament

En aquest exemple veurem finalment per a que servei la directiva **EXPOSE 80** que hem escrit en el *Dockerfile*.
Aquesta directiva informa que el container vol exposar el port 80.

Es poden engegar container que facin propagació de port sense indicar-ne cap de concret, usant l'opció *-P*. Docker 
assigna un port dinàmic al host amfitrió. Per saber quin port ha assignat s'utilitza l'ordre *docker port*. 
També amb l'ordre *docker ps* es pot observar aquesta assignació.

Crear el container amb assignació dinàmica de port
```
# Recordeu de fer el 'stop' del container si encara el teniu engegat
# docker stop web

$ docker run --rm --name web  -h web  -P -d edtasixm05/web21:minimal
079fc426f167d28ba5e855ce13d1cb4024a9f05d735cd8cf3b473022c9f58c74

$ docker ps
CONTAINER ID   IMAGE                      COMMAND                  CREATED         STATUS         PORTS                                     NAMES
079fc426f167   edtasixm05/web21:minimal   "/bin/sh -c 'apachec…"   6 seconds ago   Up 5 seconds   0.0.0.0:49153->80/tcp, :::49153->80/tcp   web

$ docker port web
80/tcp -> 0.0.0.0:49153
80/tcp -> :::49153

$ nmap 172.17.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:13 CEST
Nmap scan report for ldap.edt.org (172.17.0.2)
Host is up (0.000092s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

En l'exemple anterior podem observar que el container ha propagat el seu port 80 al port dinàmic 49153 de
totes les interfícies del host amfitrió. Observei que apereixen dues línies, una per IPv4 i una altra per IPv6.

Observar les interfícies del host amfitrió
```
$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:14 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00010s latency).
Not shown: 999 closed ports
PORT      STATE SERVICE
49153/tcp open  unknown

$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:14 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000094s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT      STATE SERVICE
49153/tcp open  unknown

$ nmap 192.168.1.45
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:14 CEST
Nmap scan report for 192.168.1.45
Host is up (0.000097s latency).
Not shown: 999 closed ports
PORT      STATE SERVICE
49153/tcp open  unknown
```

Verificar que el servei web funciona al port dinàmic
```
$ telnet 192.168.1.45 49153
Trying 192.168.1.45...
Connected to 192.168.1.45.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Thu, 05 May 2022 17:15:33 GMT
Server: Apache/2.4.53 (Debian)
Last-Modified: Thu, 05 May 2022 16:14:03 GMT
ETag: "e4-5de4607c548c0"
Accept-Ranges: bytes
Content-Length: 228
Vary: Accept-Encoding
Connection: close
Content-Type: text/html

<html>
    <head>
        <title>ASIX-M05 Dockers</title>
    </head>
    <body>
        <h1>web 1hisix</h1>
        <h2>practica M05 docker</h2>
        hola soc la pagina web
	d'exemple de ASIX-M05 dockers
    </body>
</html>
Connection closed by foreign host.
```
```
$ wget 172.17.0.1:49153
--2022-05-05 19:16:55--  http://172.17.0.1:49153/
Connecting to 172.17.0.1:49153... connected.
HTTP request sent, awaiting response... 200 OK
Length: 228 [text/html]
Saving to: ‘index.html’

index.html                              100%[=============================================================================>]     228  --.-KB/s    in 0s      

2022-05-05 19:16:55 (50.3 MB/s) - ‘index.html’ saved [228/228]
```
```
Des d'un navegador web accedir a l'adreça http://localhost:49153
```

#### Exemple-4: Propagació a determinades interfícies del host amfitrió

En tots els exemples anteriors hem vist el format on es propaga el port del container al host amfitrió
a totes les seves interfícies, però es pot seleccionar a quina interfície es vol propagar. *[Atenció, 
aquest concepte pels que no són usuals de les xarxes a vegades els costa...]*

Observem els dos exemples següents, tots dos fan el mateix, propaguen el port 80 del container al port
5000 de totes les interfícies del host amfitrió. El **wilcard 0.0.0.0** significa a totes les adreces
ip del host amfitrió.

```
 -p 0.0.0.0:5000:80
```
```
 -p 5000:80
```

Si es vol propagar el port només a una de les interfícies es pot usar aquest primer camp (que usualment 
es deixa en blanc per defecte) per indicar a quines interfícies ha d'afectar. 


**Exemple amb una sola interfície**


Així per exemple si volem que únicament es propagui al localhost faríem
```
# Recordeu de fer el 'stop' del container si encara el teniu engegat
# docker stop web

$ docker run --rm --name web  -h web  -p 127.0.0.1:5000:80 -d edtasixm05/web21:minimal
c541eef5a4611fe3daa0a6a1a955b06efa2a3b8e50e3769861d5a2a1185c7133

$ nmap 172.17.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:26 CEST
Nmap scan report for ldap.edt.org (172.17.0.2)
Host is up (0.000092s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

A les interfícies 172.17.0.2 i 192.168.1.45 ara no s'ha propagat
```
$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:26 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00012s latency).
All 1000 scanned ports on 172.17.0.1 are closed

$ nmap 192.168.1.45
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:27 CEST
Nmap scan report for 192.168.1.45
Host is up (0.000099s latency).
All 1000 scanned ports on 192.168.1.45 are closed
```

En canvi al loopback o 127.0.0.1 si que s'ha propagat (*Atenció: a vegades el nom localhost juga males passades
perquè en lloc de resoldre's per 127.0.0.1 en IPV4 es resol com a ::1 en IPv6*)
```
$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:27 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000096s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp

$ nmap 127.0.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:27 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000093s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp
```

**Exemple amb dues interfícies**

Repetim el mateix exemple propagant el port 80 del container al port 5000 del host amfitrió però
només a les interfícies del loopback 127.0.0.1 i a la del docker0 172.17.0.1, però no a la interfície
amb l'adreça IP pública 192.168.1.45.

```
# Recordeu de fer el 'stop' del container si encara el teniu engegat
# docker stop web

 docker run --rm --name web  -h web  -p 127.0.0.1:5000:80 -p 172.17.0.1:5000:80 -d edtasixm05/web21:minimal
88b759bbc4617c98d5f55e60f2ce3e98f158de67499c199ef2784f0beea65cad

$ nmap localhost
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:33 CEST
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00010s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp

$ nmap 172.17.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:34 CEST
Nmap scan report for 172.17.0.1
Host is up (0.00011s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
5000/tcp open  upnp

$ nmap 192.168.1.45
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 19:34 CEST
Nmap scan report for 192.168.1.45
Host is up (0.000099s latency).
All 1000 scanned ports on 192.168.1.45 are closed
```


