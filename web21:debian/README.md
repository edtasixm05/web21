# web21
## @edt ASIX-M05 Curs 2021-2022

### Descripció

Generar una imatge web21 amb un servidor web apache basada en debian (latest). Cal generar una
pàgina web bàsica de prova que es copia a /var/www/html.index.html. El servei s'ha d'engegar de manera que
es quedi actiu en foreground (consultar man apachectl o man apache2).

Aquesta imatge conté paquets que no són necessàris però que s'inclouen per facilitar la depuració i aprenentatge,
com són:
  * *iroute2* Permetre realitzar le sordres ip
  * *iputils-ping* Poder realitzar pings de depuració
  * *procps* Per poder realitzar les ordres ps
  * *nmap* Per poder examinar els ports


Fitxers que utilitza:
 * Dockerfile
 * index.html
 * startup.sh


#### Generar imatge

```
docker build -t edtasixm05/web21:debian .
```

#### Executar en detach

```
docker run --rm --name web -h web -d edtasixm05/web21:debian 
```

#### Verificar-ne el funcionament

Observar que està engegat el container, els processos que s'hi executen i els ports

```
$ docker run --rm --name web  -h web  -d edtasixm05/web21:debian 
6d1ddee48b7dd7ff6958c4bec3a34d4025b484df71ef34b5e7a3fa4b880f6e9b

$ docker ps
CONTAINER ID   IMAGE                     COMMAND                  CREATED         STATUS         PORTS     NAMES
6d1ddee48b7d   edtasixm05/web21:debian   "/bin/sh -c /opt/doc…"   4 seconds ago   Up 3 seconds   80/tcp    web
```

```
$ nmap 172.17.0.2
Starting Nmap 7.80 ( https://nmap.org ) at 2022-05-05 17:52 CEST
Nmap scan report for ldap.edt.org (172.17.0.2)
Host is up (0.00015s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
```

```
$ docker top web
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                13275               13254               0                   17:50               ?                   00:00:00            /bin/sh -c /opt/docker/startup.sh
root                13311               13275               0                   17:50               ?                   00:00:00            /bin/bash /opt/docker/startup.sh
root                13314               13311               0                   17:50               ?                   00:00:00            /bin/sh /usr/sbin/apachectl -k start -X
33                  13323               13314               0                   17:50               ?                   00:00:00            /usr/sbin/apache2 -k start -X
```

Descarregar la pàgina web

```
$ wget 172.17.0.2 
--2022-05-05 17:55:01--  http://172.17.0.2/
Connecting to 172.17.0.2:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 228 [text/html]
Saving to: ‘index.html.1’

index.html.1                            100%[=============================================================================>]     228  --.-KB/s    in 0s      

2022-05-05 17:55:01 (18.7 MB/s) - ‘index.html.1’ saved [228/228]
```

```
$ telnet 172.17.0.2 80
Trying 172.17.0.2...
Connected to 172.17.0.2.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Thu, 05 May 2022 15:56:47 GMT
Server: Apache/2.4.53 (Debian)
Last-Modified: Thu, 05 May 2022 15:50:11 GMT
ETag: "e4-5de45b26f0743"
Accept-Ranges: bytes
Content-Length: 228
Vary: Accept-Encoding
Connection: close
Content-Type: text/html

<html>
    <head>
        <title>ASIX-M05 Dockers</title>
    </head>
    <body>
        <h1>web 1hisix</h1>
        <h2>practica M05 docker</h2>
        hola soc la pagina web
	d'exemple de ASIX-M05 dockers
    </body>
</html>
Connection closed by foreign host.
```

```
Des d'un navegador web accedir a l'adreça: 172.17.0.2
```

### Aturar el container del servei web

Recordeu d'aturar el container i eliminar-lo (es fa automàticament si s'ha fet el run amb --rm) per poder tornar a 
engegar-ne un amb el mateix nom.

```
$ docker stop web
web

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

```

--- 

#### Procés de depuració si no funciona

Primerament engegar-la interactivament, des d'un shell observar els processos. Executar manualment startup 
i observar si engega o no el servei en forground. **Atenció** per poder depurar una imatge en detach cal pensar 
a canviar la opció *-d* per *-it* sinó el container s'executa en background i no podem treballar-hi.

```
$ docker run --rm --name web -h web -it edtasixm05/web21:debian  /bin/bash
root@debian:/opt/docker# ps ax
    PID TTY      STAT   TIME COMMAND
      1 pts/0    Ss     0:00 /bin/bash
     10 pts/0    R+     0:00 ps ax

root@debian:/opt/docker# apt-get install vim

root@debian:/opt/docker# whereis apache2
apache2: /usr/sbin/apache2 /usr/lib/apache2 /etc/apache2 /usr/share/apache2 /usr/share/man/man8/apache2.8.gz

root@debian:/opt/docker# bash startup.sh 
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message
```

Eliminar les verions d'imatges que siguin velles, assegureu-vos de fer neteja de les imatges i dels 
containers, sovint executem una cosa diferent de la que ens pensem.

```
$ docker images
REPOSITORY                    TAG       IMAGE ID       CREATED              SIZE
edtasixm05/web21              debian    f5d7e224f473   About a minute ago   293MB
<none>                        <none>    c8fd950d130a   2 minutes ago        293MB
<none>                        <none>    4c0627ee2ddb   18 minutes ago       293MB
debian                        latest    82bd5ee7b1c5   8 months ago         124MB
gcr.io/k8s-minikube/kicbase   v0.0.23   9fce26cb202e   11 months ago        1.09GB
kalilinux/kali-rolling        latest    b287d63d3378   11 months ago        125MB
fedora                        32        c451de0d2441   12 months ago        202MB
edtasixm11/tls18              ldaps     58591b87e630   3 years ago          455MB

$ docker rmi c8fd950d130a 4c0627ee2ddb 
```

Examinar la imatge generada:
```
$ docker history edtasixm05/web21:debian 
IMAGE          CREATED          CREATED BY                                      SIZE      COMMENT
e0b427dd80e7   53 seconds ago   /bin/sh -c #(nop)  EXPOSE 80                    0B        
30e5aa8d495f   53 seconds ago   /bin/sh -c #(nop)  CMD ["/bin/sh" "-c" "/opt…   0B        
38189b324d1b   54 seconds ago   /bin/sh -c #(nop) WORKDIR /opt/docker           0B        
cd8b96ee811e   54 seconds ago   /bin/sh -c chmod +x /opt/docker/startup.sh      77B       
020a93980d4f   54 seconds ago   /bin/sh -c #(nop) COPY multi:c7f411a6171f589…   6.96kB    
df77787f1997   34 minutes ago   /bin/sh -c mkdir /opt/docker                    0B        
9b84d8908f53   34 minutes ago   /bin/sh -c apt-get update && apt-get -y inst…   169MB     
b65141538e08   37 minutes ago   /bin/sh -c #(nop)  LABEL author=@edt ASIX M05   0B        
2a3f815efd5f   37 minutes ago   /bin/sh -c #(nop)  LABEL subject=webserver      0B        
82bd5ee7b1c5   8 months ago     /bin/sh -c #(nop)  CMD ["bash"]                 0B        
<missing>      8 months ago     /bin/sh -c #(nop) ADD file:1fedf68870782f1b4…   124MB     
```




