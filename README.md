# web21
## @edt ASIX-M05 Curs 2021-2022


### Exemples de creació d'imatges de servei web

 * **edtasixm05/web21:base** Imatge per practicar inicialment la creació
   de containers interactivament.

 * **edtasixm05/web21:detach** Imatge bàsica amb un servidor web detach 
   basat en *fedora27* i amb una pàgina d'inici simple.

 * **edtasixm05/web21:debian** Imatge d'un servidor web detach basada en debian.

 * **edtasixm05/web21:develop** Imatge d'un servidor web detach amb el diretori
   de publicació 'muntat' el host amfitrió com un directori o com un volum.
 
### Altres exemples


#### Entrypoint / CMD

  * **entry-cmd-net** Exemples fets per usar en les explicacions dels conceptes
    *entrypoint*, *cmd* i *network*.

   * **cmd** imatge amb un cmd *date* per observar el funcionament de cmd.

   * **entry** imatge amb un entrypoint *date* per observar el funcionament de entry.

   * **entryopc** Afegit un exemple amb un script *opcions.sh* d'entrypoint per fer dia,
     o sysinfo o calendari.

   * **entrybd**  Afegit un escript d'exemple de entrypoint que simula una imatge amb 
     un gestor de base de dades amb les opcions initdb, destroy, dump, start.


#### Network

  * **ssh** Imatge servidor/client ssh amb algunes utilitats de xarxa.


